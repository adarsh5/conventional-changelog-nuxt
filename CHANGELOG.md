---
website: https://psychx86.com
---
# Psych x86 Technologies LLP. ®

## [1.5.0](https://gitlab.com/adarsh5/conventional-changelog-nuxt/compare/v1.4.0...v1.5.0) (2020-02-18)


### Features

* **lottie:** added lottie light with functionality as a global component ([4592214](https://gitlab.com/adarsh5/conventional-changelog-nuxt/commit/45922142bb19a139f7f8b1ae162dee41ccc292fe))


### Bug Fixes

* changelog now center aligned ([68a1392](https://gitlab.com/adarsh5/conventional-changelog-nuxt/commit/68a13924acabdd6de06c950a81d21bb2494469d3))

## [1.4.0](https://gitlab.com/adarsh5/conventional-changelog-nuxt/compare/v1.3.0...v1.4.0) (2020-02-17)


### Features

* added mutiple styles for html renderer. ([305c032](https://gitlab.com/adarsh5/conventional-changelog-nuxt/commit/305c03258575f6c9ba41d57922f0f9e1495089c5))

## [1.3.0](https://gitlab.com/adarsh5/conventional-changelog-nuxt/compare/v1.2.1...v1.3.0) (2020-02-16)


### Features

* parsed changelog as html, added basic styling with css ([8da2db3](https://gitlab.com/adarsh5/conventional-changelog-nuxt/commit/8da2db3bd9ba247d8462f1fe78d54f700b79ccd6))

### [1.2.1](https://gitlab.com/adarsh5/conventional-changelog-nuxt/compare/v1.2.0...v1.2.1) (2020-02-16)

## [1.2.0](///compare/v1.1.0...v1.2.0) (2020-02-16)


### Features

* **project:** added changelog header 7ffc432

## 1.1.0 (2020-02-16)


### Features

* **project:** added conventional commit standard version! Voila! 34cf8ac
